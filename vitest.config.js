import vue from "@vitejs/plugin-vue";
import vueJsx from "@vitejs/plugin-vue-jsx";
import {defineConfig} from "vitest/config";

export default defineConfig({
    test: {
        globals: true,
        environment: 'jsdom',
        coverage: {
            enabled: true,
            provider: 'v8',
            reporter: ['text-summary', 'text', 'html', 'json'],
            reporters: ['text-summary', 'text', 'html', 'json']
        },
    },
    plugins: [vue(), vueJsx()],
});