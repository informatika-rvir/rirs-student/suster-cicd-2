import { shallowMount, mount } from '@vue/test-utils';
import NotificationsOverviewView from '../../src/views/notifications/NotificationsOverviewView.vue';
import {createPinia, setActivePinia} from 'pinia';

import {vi, describe, beforeEach, it} from 'vitest';
import {useNotificationsStore} from "../../src/stores/notifications/notifications.ts";


describe('NotificationsOverviewView', () => {
    let pinia;
    let wrapper;

    beforeEach(() => {
        pinia = createPinia();
        setActivePinia(pinia);
        const notificationsStore = useNotificationsStore();
        notificationsStore.addNotification({
                title: 'Notification 1',
                datePosted: new Date('2023-01-01'),
                description: 'Description 1',
            });
        wrapper = shallowMount(NotificationsOverviewView, {
            global: {
                plugins: [pinia]
            }
        });
    });

    it('renders', () => {
        console.log(wrapper.html());
        expect(wrapper.exists()).toBe(true);
    });

    it('displays the title', () => {
        expect(wrapper.find('h1').text()).toBe('Notifications');
    });

    // TODO: Fix this test
    it('displays notifications', async () => {
        await wrapper.vm.$nextTick();
        const notifications = wrapper.findAll('.grid-item');
        expect(notifications.length).toBe(1);
    });

    it('displays notification title', () => {
        const notificationTitle = wrapper.find('.grid-item h4 u i');
        expect(notificationTitle.text()).toBe('1. Notification 1');
    });

    it('displays notification date', () => {
        const notificationDate = wrapper.find('.grid-item i');
        expect(notificationDate.text()).toBe('01. 01. 2023');
    });

    it('displays notification description', () => {
        const notificationDescription = wrapper.find('.grid-item p');
        expect(notificationDescription.text()).toBe('Description 1');
    });

    it('does not display extra space for last item', () => {
        const extraSpace = wrapper.find('.grid-item div');
        expect(extraSpace.exists()).toBe(false);
    });

    it('formats date correctly', () => {
        const formattedDate = wrapper.vm.formatDate(new Date('2023-01-01'));
        expect(formattedDate).toBe('01. 01. 2023');
    });

    it('checks if item is last', () => {
        const isLastItem = wrapper.vm.isLastItem(0); // Assuming there is only one item
        expect(isLastItem).toBe(true);
    });

    it('renders with no notifications', () => {
        expect(wrapper.exists()).toBe(true);

        const noNotificationsMessage = wrapper.find('.no-notifications-message');
        expect(noNotificationsMessage.exists()).toBe(true);
        expect(noNotificationsMessage.text()).toBe('No notifications available.');
    });

    it('updates searchTerm on input change', async () => {
        const searchInput = wrapper.find('input');
        await searchInput.setValue('New Search Term');

        expect(wrapper.vm.searchTerm).toBe('New Search Term');
    });

    it('filters notifications based on searchTerm', async () => {
        wrapper.vm.searchTerm = '1. New Employee Onboarding';

        await wrapper.vm.$nextTick();

        const notifications = wrapper.findAll('.grid-item');
        expect(notifications.length).toBe(1);
        const notificationTitle = wrapper.find('.grid-item h4 u i');
        expect(notificationTitle.text()).toBe('1. New Employee Onboarding');
    });

    it('displays a back button', () => {
        const backButton = wrapper.find('.back-button button');
        expect(backButton.exists()).toBe(true);
    });

    it('displays the correct text on the back button', () => {
        const backButton = wrapper.find('.back-button button');
        expect(backButton.text()).toBe('< Back');
    });

    it('displays the "Refresh Notifications" button', () => {
        const refreshButton = wrapper.find('.refresh-button button');
        expect(refreshButton.exists()).toBe(true);
    });

    it('calls refreshNotifications method when "Refresh Notifications" button is clicked', async () => {
        const refreshNotificationsSpy = vi.spyOn(wrapper.vm, 'refreshNotifications');
        await wrapper.find('button').trigger('click');
        expect(refreshNotificationsSpy).toHaveBeenCalled();
    });
});

