FROM node:20-alpine

WORKDIR /app

COPY . .

RUN npm i
RUN npm i -g serve

RUN npm run build

ENV NODE_ENV production

EXPOSE 3000

CMD ["npx", "serve", "dist", "-l", "3000"]

