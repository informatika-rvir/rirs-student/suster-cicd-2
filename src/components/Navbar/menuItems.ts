export const menuItems = [
    {
        label: 'Home',
        icon: 'pi pi-fw pi-home',
        route: '/',
    },
    {
        label: 'Employees',
        icon: 'pi pi-fw pi-users',
        items: [
            {
                label: 'Overview',
                icon: 'pi pi-fw pi-list',
                route: '/employees/overview',
            },
            {
                label: 'Add employee',
                icon: 'pi pi-fw pi-plus',
                route: '/employees/add',
            },
        ],
    },
    {
        label: 'Salary',
        icon: 'pi pi-fw pi-money-bill',
        items: [
            {
                label: 'View all salaries',
                icon: 'pi pi-fw pi-list',
                route: '/salary/overview',
            },
        ],
    },
    {
        label: 'Notifications',
        icon: 'pi pi-fw pi-bell',
        items: [
            {
                label: 'Overview',
                icon: 'pi pi-fw pi-list',
                route: '/notifications/overview',
            },
            {
                label: 'Add notification',
                icon: 'pi pi-fw pi-plus',
                route: '/notifications/add',
            },
        ],
    },
];
