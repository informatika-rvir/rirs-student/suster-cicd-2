import type { Employee } from './employee';

export const employeeCollection: Employee[] = [
  {
    name: 'John',
    surname: 'Doe',
    address: '123 Main Street, City, Country',
    employmentDate: '2023-01-15',
  },
  {
    name: 'Alice',
    surname: 'Smith',
    address: '456 Elm Avenue, Town, Country',
    employmentDate: '2023-02-20',
  },
  {
    name: 'Michael',
    surname: 'Johnson',
    address: '789 Oak Road, Village, Country',
    employmentDate: '2023-03-10',
  },
  {
    name: 'Sarah',
    surname: 'Wilson',
    address: '101 Pine Lane, Suburb, Country',
    employmentDate: '2023-04-05',
  },
  {
    name: 'Robert',
    surname: 'Brown',
    address: '222 Cedar Street, Town, Country',
    employmentDate: '2023-05-15',
  },
  {
    name: 'Emily',
    surname: 'Anderson',
    address: '333 Birch Avenue, City, Country',
    employmentDate: '2023-06-20',
  },
  {
    name: 'David',
    surname: 'Martin',
    address: '444 Maple Road, Suburb, Country',
    employmentDate: '2023-07-10',
  },
  {
    name: 'Olivia',
    surname: 'Lee',
    address: '555 Walnut Lane, Village, Country',
    employmentDate: '2023-08-05',
  },
  {
    name: 'James',
    surname: 'Garcia',
    address: '666 Cherry Street, City, Country',
    employmentDate: '2023-09-15',
  },
  {
    name: 'Emma',
    surname: 'Rodriguez',
    address: '777 Rose Avenue, Town, Country',
    employmentDate: '2023-10-20',
  },
];
