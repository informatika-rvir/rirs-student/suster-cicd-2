import { ref, type Ref } from 'vue';
import { defineStore } from 'pinia';
import type { Employee } from './employee';
import { employeeCollection } from './employeesInitialData';

export const useEmployeesStore = defineStore('employees', () => {
  const employees: Ref<Employee[]> = ref(employeeCollection);

  function addEmployee(employee: Employee): void {
    employees.value.push(employee);
  }

  return { employees, addEmployee };
});
