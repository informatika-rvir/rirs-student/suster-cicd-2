export interface Employee {
  name: string;
  surname: string;
  address: string;
  employmentDate: string;
}
