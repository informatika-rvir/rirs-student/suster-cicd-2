export interface Notification {
  title: string;
  description: string;
  datePosted: Date;
}
