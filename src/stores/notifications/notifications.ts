import { type Ref, ref } from 'vue';
import { defineStore } from 'pinia';
import type { Notification } from './notification';
import { notificationsCollection } from './notificationsInitialData';

export const useNotificationsStore = defineStore('notifications', () => {
  const notifications: Ref<Notification[]> = ref(notificationsCollection);
  function addNotification(notification: Notification): void {
    notifications.value.unshift(notification);
  }

  function removeNotification(index: number): void {
    notifications.value.splice(index, 1);
  }

  return { notifications, addNotification, removeNotification };
});
