import type { Notification } from './notification';

export const notificationsCollection: Notification[] = [
  {
    title: 'New Employee Onboarding',
    description:
      'Please welcome our new team member, John Smith, who will be joining our company on November 10th, 2023. Make sure to provide him with a warm welcome and all the necessary resources for a smooth onboarding process.',
    datePosted: new Date('2023-11-01'),
  },
  {
    title: 'Upcoming Training Session',
    description:
      "Don't forget the upcoming training session on workplace safety scheduled for November 15th, 2023, from 9:00 AM to 12:00 PM in the conference room. All employees are required to attend.",
    datePosted: new Date('2023-11-03'),
  },
  {
    title: 'Performance Review Reminder',
    description:
      "It's that time of the year again! Performance reviews are due by December 1st, 2023. Please ensure that you have completed your self-assessment and gathered feedback from your peers for a constructive review discussion.",
    datePosted: new Date('2023-10-25'),
  },
  {
    title: 'Holiday Party Announcement',
    description:
      "Get ready for the annual company holiday party on December 15th, 2023, from 6:00 PM to 10:00 PM at the Grand Ballroom. We have exciting activities and prizes lined up for you. Don't miss out on the festive fun!",
    datePosted: new Date('2023-11-02'),
  },
  {
    title: 'Remote Work Policy Update',
    description:
      'We have updated our remote work policy to provide more flexibility for our employees. Please review the changes in the company handbook, and if you have any questions or concerns, reach out to HR for clarification.',
    datePosted: new Date('2023-10-29'),
  },
  {
    title: 'Employee of the Month',
    description:
      'We are proud to announce that Sarah Johnson has been awarded the Employee of the Month for her outstanding contributions to the company. Congratulations, Sarah, on this well-deserved recognition!',
    datePosted: new Date('2023-11-05'),
  },
  {
    title: 'Product Launch Meeting',
    description:
      'Join us for the product launch meeting on November 20th, 2023, where we will unveil our latest product line. Be prepared for an exciting presentation and updates on marketing strategies.',
    datePosted: new Date('2023-11-06'),
  },
  {
    title: 'Team Building Event',
    description:
      "Mark your calendar for the team building event on November 25th, 2023. We have planned fun activities to foster team spirit and collaboration. Don't miss this opportunity to bond with your colleagues.",
    datePosted: new Date('2023-11-08'),
  },
  {
    title: 'Company Town Hall Meeting',
    description:
      "The next company town hall meeting is scheduled for November 30th, 2023. Senior leadership will provide updates on the company's performance and future plans. Your questions and feedback are welcome.",
    datePosted: new Date('2023-11-10'),
  },
];
