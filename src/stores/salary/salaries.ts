import {type Ref, ref} from "vue";
import {defineStore} from 'pinia';
import type {Salary} from "./salary";
import {salariesCollection} from "./salariesInitialData"

export const useSalariesStore = defineStore('salaries', () => {
    const salaries:Ref<Salary[]> = ref(salariesCollection);

    return {salaries};
});