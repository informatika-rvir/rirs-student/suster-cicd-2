import type { Salary } from './salary'

export const salariesCollection: Salary[] = [
    {
        date: new Date('2023-01-14'),
        amount: 1850,
        additionalDescription: 'Salary for month of Jan.',
    },
    {
        date: new Date('2023-02-14'),
        amount: 1900,
        additionalDescription: 'Salary for month of Feb.',
    },
    {
        date: new Date('2023-03-14'),
        amount: 1950,
        additionalDescription: 'Salary for month of Mar.',
    },
    {
        date: new Date('2023-04-14'),
        amount: 2000,
        additionalDescription: 'Salary for month of Apr.',
    },
    {
        date: new Date('2023-05-14'),
        amount: 2050,
        additionalDescription: 'Salary for month of May.',
    },
    {
        date: new Date('2023-06-14'),
        amount: 2100,
        additionalDescription: 'Salary for month of Jun.',
    },
    {
        date: new Date('2023-07-14'),
        amount: 2150,
        additionalDescription: 'Salary for month of Jul.',
    },
    {
        date: new Date('2023-08-14'),
        amount: 2200,
        additionalDescription: 'Salary for month of Aug.',
    },
    {
        date: new Date('2023-09-14'),
        amount: 2250,
        additionalDescription: 'Salary for month of Sep.',
    },
    {
        date: new Date('2023-10-14'),
        amount: 2300,
        additionalDescription: 'Salary for month of Oct.',
    },
    {
        date: new Date('2023-11-14'),
        amount: 2350,
        additionalDescription: 'Salary for month of Nov.',
    },
    {
        date: new Date('2023-12-14'),
        amount: 2400,
        additionalDescription: 'Salary for month of Dec.',
    },
];

