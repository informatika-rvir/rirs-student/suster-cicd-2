export interface Salary {
    date: Date;
    amount: number;
    additionalDescription: string;
}
