import HomeView from '../views/HomeView.vue';
import EmployeesOverviewView from '../views/employees/EmployeesOverviewView.vue';
import AddEmployeeView from '../views/employees/AddEmployeeView.vue';
import AddNotificationView from '../views/notifications/AddNotificationView.vue';
import NotificationsOverviewView from '../views/notifications/NotificationsOverviewView.vue';
import SalaryOverviewView from "../views/salary/SalaryOverviewView.vue";
import {createRouter, createWebHistory} from "vue-router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
    },
    {
      path: '/employees/overview',
      name: 'employeesOverview',
      component: EmployeesOverviewView,
    },
    {
      path: '/employees/add',
      name: 'addEmployee',
      component: AddEmployeeView,
    },
    {
      path: '/notifications/overview',
      name: 'notificationsOverview',
      component: NotificationsOverviewView,
    },
    {
      path: '/notifications/add',
      name: 'addNotification',
      component: AddNotificationView,
    },
    {
      path: '/salary/overview',
      name: 'salaryOverview',
      component: SalaryOverviewView,
    },
  ],
});

export default router;
