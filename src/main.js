import { createApp } from 'vue';

import App from './App.vue';
import router from './router';
import PrimeVue from 'primevue/config';

import 'primevue/resources/themes/lara-dark-blue/theme.css';
import '/node_modules/primeflex/primeflex.css';

import { createPinia } from 'pinia';

const app = createApp(App);
const pinia = createPinia();

app.use(createPinia());
app.use(PrimeVue, { inputStyle: 'filled' });
app.use(router);
app.use(pinia);

app.mount('#app');
